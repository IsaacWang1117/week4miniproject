use axum::{routing::get, Json, Router};
use rust_axum_random_fruit_microservice::random_fruit;
use serde_json::json;

//Root Route for Change Machine
async fn root() -> &'static str {
    "
    Fruit Machine

    **Primary Route:**
    /fruit
    "
}

// Adapted fruit function for Axum
async fn fruit() -> impl axum::response::IntoResponse {
    let fruit = random_fruit(); // Assuming this is correctly imported from lib.rs
    let json = json!({
     "Random Fruit": fruit
    });
    Json(json)
}



#[tokio::main]
async fn main() {
    // Create the router
    let app = Router::new()
        .route("/", get(root))
        .route("/fruit", get(fruit));

    // Define the address and run the server
    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    axum::serve(listener, app).await.unwrap();

}