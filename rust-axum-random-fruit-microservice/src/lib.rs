/*A library that returns back random fruit */

use rand::Rng;

//create an const array of 7 fruits
pub const FRUITS: [&str; 7] = [
    "Apple",
    "Banana",
    "Pineapple",
    "Strawberry",
    "Watermelon",
    "Grapes",
    "Mango",
];

//create a function that returns a random fruit
pub fn random_fruit() -> &'static str {
    let mut rng = rand::thread_rng();
    let random_index = rng.gen_range(0..FRUITS.len());
    FRUITS[random_index]
}


//test

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn random_fruit_is_non_empty() {
        let fruit = random_fruit();
        assert!(!fruit.is_empty(), "The random fruit should not be empty.");
    }

    #[test]
    fn random_fruit_is_in_list() {
        let fruit = random_fruit();
        assert!(
            FRUITS.contains(&fruit),
            "The random fruit should be one of the predefined fruits."
        );
    }
}