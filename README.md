# Week4MiniProject

## Overview
This project demonstrates the containerization of a simple Rust Actix web application that outputs random fruit names. The application is packaged into a Docker container, which can be built and run locally.

## Installation and Getting Started

* Docker installed and running on my machine
* Git for repository cloning

### Clone the Repository and Edit the files

```bash
git clone git@gitlab.com:dukeaiml/duke-coursera-labs/rust-axum-greedy-coin-microservice.git
```

### Modify the Folder and Create a Random Fruit Function
Reference the rust-mlops-template to create a function in the Rust application that provides a random fruit name.

### Build and Run the Service Locally
Inside the project directory, build the project using Cargo to ensure it compiles successfully

```bash
cargo build

cargo run
```

### Test the Microservice
Verify that the microservice is functioning correctly by sending a request to the running application

```bash
curl http://localhost:3000/fruit
```

### Build the Docker Image
With Docker Desktop running, build the Docker image from the Dockerfile
```bash
docker build -t myimage .
```
![Docker image built](./rust-axum-random-fruit-microservice/docker_image_built.png)

### Run the Docker Container

Run the Docker container from the image

```bash
docker run -dp 3000:3000 myimage
```

### Test the Docker Container


![Where Docker run](./rust-axum-random-fruit-microservice/where_does_docker_run.png)
![Test](./rust-axum-random-fruit-microservice/docker_test_2.png)
